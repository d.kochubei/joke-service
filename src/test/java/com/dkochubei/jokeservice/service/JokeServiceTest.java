package com.dkochubei.jokeservice.service;

import com.dkochubei.jokeservice.controller.dto.JokeDto;
import com.dkochubei.jokeservice.service.client.JokeClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JokeServiceTest {

    @Mock
    private JokeClient jokeServiceClient;
    @InjectMocks
    private JokeService jokeService;

    @Test
    void getRandomJokes_whenRequestZeroJokes_returnEmptyList() {
        JokeDto joke = prepareJoke();
        when(jokeServiceClient.getRandomJoke()).thenReturn(joke);

        var actual = jokeService.getRandomJokes(35);

        verify(jokeServiceClient, times(35)).getRandomJoke();
        assertEquals(35, actual.size());
    }

    @Test
    void getRandomJokes_whenRequestOneJoke_returnOnlyJoke() {
        JokeDto joke = prepareJoke();
        when(jokeServiceClient.getRandomJoke()).thenReturn(joke);

        var actual = jokeService.getRandomJokes(1);

        verify(jokeServiceClient, times(1)).getRandomJoke();
        assertEquals(1, actual.size());
    }

    @Test
    void getRandomJokes_whenRequestHundredJokes_returnHundredJokes() {
        JokeDto joke = prepareJoke();
        when(jokeServiceClient.getRandomJoke()).thenReturn(joke);

        var actual = jokeService.getRandomJokes(100);

        verify(jokeServiceClient, times(100)).getRandomJoke();
        assertEquals(100, actual.size());
    }

    @Test
    void getRandomJokes_whenRequestMultipleJokes_returnExactNumberOfJokes() {
        JokeDto joke = prepareJoke();
        when(jokeServiceClient.getRandomJoke()).thenReturn(joke);

        var actual = jokeService.getRandomJokes(35);

        verify(jokeServiceClient, times(35)).getRandomJoke();
        assertEquals(35, actual.size());
    }

    private static JokeDto prepareJoke() {
        var joke = new JokeDto();
        joke.setId(1L);
        joke.setType("type");
        joke.setSetup("setup");
        joke.setPunchline("punchline");
        return joke;
    }
}