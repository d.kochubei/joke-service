package com.dkochubei.jokeservice.controller;

import com.dkochubei.jokeservice.controller.dto.JokeDto;
import com.dkochubei.jokeservice.service.client.JokeClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class JokeControllerTest {

    @MockBean
    private JokeClient jokeClient;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getJokes_whenRequestZeroJokes_returnBadRequest() {
        var actual = restTemplate.exchange(
                "/api/joke-service/jokes?amount=0", HttpMethod.GET, null, Void.class
        );

        assertThat(actual.getStatusCode().is4xxClientError()).isTrue();
    }

    @Test
    public void getJokes_whenRequestMoreThanHundredJokes_returnBadRequest() {
        var actual = restTemplate.exchange(
                "/api/joke-service/jokes?amount=101", HttpMethod.GET, null, Void.class
        );

        assertThat(actual.getStatusCode().is4xxClientError()).isTrue();
    }

    @Test
    public void getJokes_whenRequestFewJokes_returnJokes() {
        var joke1 = prepareJoke(1L);
        var joke2 = prepareJoke(2L);
        var joke3 = prepareJoke(3L);

        when(jokeClient.getRandomJoke()).thenReturn(joke1)
                .thenReturn(joke2)
                .thenReturn(joke3);

        var actual = restTemplate.exchange(
                "/api/joke-service/jokes?amount=3", HttpMethod.GET, null, JokeDto[].class
        );

        assertThat(actual.getBody()).hasSize(3);
        assertThat(actual.getBody()).containsExactlyInAnyOrder(joke1, joke2, joke3);

    }

    private static JokeDto prepareJoke(Long id) {
        var joke = new JokeDto();
        joke.setId(id);
        joke.setType("type");
        joke.setSetup("setup");
        joke.setPunchline("punchline");
        return joke;
    }
}