package com.dkochubei.jokeservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class JokesBadRequestException extends RuntimeException {
    public JokesBadRequestException() {
        super("Amount of jokes must be between 1 and 100");
    }
}
