package com.dkochubei.jokeservice.controller.dto;

import lombok.Data;

@Data
public class JokeDto {
    private Long id;
    private String type;
    private String setup;
    private String punchline;
}
