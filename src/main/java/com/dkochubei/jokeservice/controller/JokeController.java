package com.dkochubei.jokeservice.controller;

import com.dkochubei.jokeservice.controller.dto.JokeDto;
import com.dkochubei.jokeservice.exception.JokesBadRequestException;
import com.dkochubei.jokeservice.service.JokeService;
import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/joke-service/")
public class JokeController {

    @Value("${jokes.amount.default:5}")
    private Integer defaultAmountOfJokes;

    private final JokeService jokeService;

    @GetMapping("/jokes")
    public List<JokeDto> jokes(@PathParam("amount") Integer amount) {
        if (amount == null) amount = defaultAmountOfJokes;
        if (amount < 1 || amount > 100) {
            log.warn("Amount of jokes must be between 1 and 100");
            throw new JokesBadRequestException();
        }

        return jokeService.getRandomJokes(amount);
    }

}
