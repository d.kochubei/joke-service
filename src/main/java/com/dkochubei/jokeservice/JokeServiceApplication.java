package com.dkochubei.jokeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class JokeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JokeServiceApplication.class, args);
	}

}
