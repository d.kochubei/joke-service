package com.dkochubei.jokeservice.service.client;

import com.dkochubei.jokeservice.controller.dto.JokeDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "joke-service", url = "${joke-service.url}")
public interface JokeClient {
    @RequestMapping(method = RequestMethod.GET, value = "/random_joke")
    JokeDto getRandomJoke();
}
