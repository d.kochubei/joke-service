package com.dkochubei.jokeservice.service;

import com.dkochubei.jokeservice.controller.dto.JokeDto;
import com.dkochubei.jokeservice.service.client.JokeClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.util.concurrent.CompletableFuture.allOf;
import static java.util.concurrent.CompletableFuture.supplyAsync;

@Slf4j
@Service
@RequiredArgsConstructor
public class JokeService {
    private final JokeClient jokeServiceClient;

    public List<JokeDto> getRandomJokes(Integer amount) {
        log.info("Get {} jokes", amount);
        if (amount == null || amount < 1) return List.of();

        var jokes = new ArrayList<JokeDto>();
        var iterations = amount / 10;

        while (iterations > 0) {
            var tenJokes = getJokes(10);
            jokes.addAll(tenJokes);
            iterations--;
        }

        var remainingNumberOfJokes = amount % 10;
        var lastJokes = getJokes(remainingNumberOfJokes);
        jokes.addAll(lastJokes);
        return jokes;
    }

    private List<JokeDto> getJokes(Integer amount) {
        var futures = new ArrayList<CompletableFuture<JokeDto>>();

        while (amount > 0) {
            futures.add(supplyAsync(jokeServiceClient::getRandomJoke));
            amount--;
        }

        allOf(futures.toArray(CompletableFuture[]::new)).join();

        log.info("Successfully got {} jokes", amount);
        return futures.stream()
                .map(JokeService::getJokeDto)
                .toList();
    }

    private static JokeDto getJokeDto(CompletableFuture<JokeDto> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            log.error("Error when requesting a joke", e);
            throw new RuntimeException("Error when requesting a joke", e);
        }
    }
}
